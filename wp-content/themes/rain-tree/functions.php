<?php


add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

add_action( 'wp_enqueue_scripts', function(){
	wp_enqueue_style( 'style-name', get_stylesheet_directory_uri()."/style.css", array(), "", false);
	wp_enqueue_style( 'light-gallery-style', get_stylesheet_directory_uri()."/include/lightgallery-all.min.css", array(), "", false);
	wp_enqueue_script("light-gallery-js",get_stylesheet_directory_uri()."/include/lightgallery-all.min.js","","",1);
});

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab

    return $tabs;

}


// Hiding Prices on all Free Sample Products
add_filter( 'woocommerce_get_price_html', function( $price, $product ) {
	if ( is_admin() ) return $price;
	// Hide for these category slugs / IDs
	$hide_for_categories = array( 'free-samples' );
	// Don't show price when its in one of the categories
	if ( has_term( $hide_for_categories, 'product_cat', $product->get_id() ) ) {
		return '';
	}
	return $price; // Return original price
}, 10, 2 );
add_filter( 'woocommerce_cart_item_price', '__return_false' );
add_filter( 'woocommerce_cart_item_subtotal', '__return_false' );



/**
 * Remove Categories from WooCommerce Product Category Widget
 *
 * @author   Ren Ventura
 */

//* Used when the widget is displayed as a dropdown
add_filter( 'woocommerce_product_categories_widget_dropdown_args', 'rv_exclude_wc_widget_categories' );
 //* Used when the widget is displayed as a list
add_filter( 'woocommerce_product_categories_widget_args', 'rv_exclude_wc_widget_categories' );
function rv_exclude_wc_widget_categories( $cat_args ) {
	$cat_args['exclude'] = array('127','99','100','101','166'); // Insert the product category IDs you wish to exclude
	return $cat_args;
}

/*add_action( 'woocommerce_before_order_notes', 'bbloomer_add_custom_checkout_field' );
  
function bbloomer_add_custom_checkout_field( $checkout ) { 
   
   $saved_license_no = "sampleRequest";
   woocommerce_form_field( 'formType', array(        
      'type' => 'hidden',        
       
      'default' => $saved_license_no,        
   ), $checkout->get_value( 'license_no' ) ); 
}*/
add_action( 'woocommerce_before_order_notes', 'additional_hidden_checkout_field' );
function additional_hidden_checkout_field() {
    echo '<input type="hidden" name="promoCode" value="smplrqsttrspns">';
    echo '<input type="hidden" name="formType" value="sampleRequest">';
}
/**
 * @snippet       Save & Display Custom Field @ WooCommerce Order
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.8
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
add_action( 'woocommerce_checkout_update_order_meta', 'bbloomer_save_new_checkout_field' );
  
function bbloomer_save_new_checkout_field( $order_id ) { 
    if ( $_POST['formType'] ) update_post_meta( $order_id, '_formType', esc_attr( $_POST['formType'] ) );
	if ( $_POST['promoCode'] ) update_post_meta( $order_id, '_promoCode', esc_attr( $_POST['promoCode'] ) );
}

add_filter( 'body_class', function( $classes ) {
    $allow_page_id =array("1339", "382", "4497", "4286", "4292", "1265003");
	if(in_array(get_the_ID() , $allow_page_id)){
		$classes = array_merge( $classes, array( 'custombody_changes' ) );
	}
    return $classes;

} );


/*
* Creating a function to create our CPT
*/
 
function custom_post_type_kitchen() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Kitchens Gallery', 'Post Type General Name', 'twentytwenty' ),
        'singular_name'       => _x( 'Kitchens', 'Post Type Singular Name', 'twentytwenty' ),
        'menu_name'           => __( 'Kitchens Gallery', 'twentytwenty' ),
        'parent_item_colon'   => __( 'Parent Kitchens', 'twentytwenty' ),
        'all_items'           => __( 'All Kitchens', 'twentytwenty' ),
        'view_item'           => __( 'View Kitchens', 'twentytwenty' ),
        'add_new_item'        => __( 'Add New Kitchens', 'twentytwenty' ),
        'add_new'             => __( 'Add New', 'twentytwenty' ),
        'edit_item'           => __( 'Edit Kitchens', 'twentytwenty' ),
        'update_item'         => __( 'Update Kitchens', 'twentytwenty' ),
        'search_items'        => __( 'Search Kitchens', 'twentytwenty' ),
        'not_found'           => __( 'Not Found', 'twentytwenty' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Kitchens', 'twentytwenty' ),
        'description'         => __( 'Kitchens Gallery', 'twentytwenty' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
     
    // Registering your Custom Post Type
    register_post_type( 'Kitchens', $args );
 
}
add_action( 'init', 'custom_post_type_kitchen', 0 ); 


/*
* Creating a function to create our CPT
*/
 
function custom_post_type_living() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Living Rooms Gallery', 'Post Type General Name', 'twentytwenty' ),
        'singular_name'       => _x( 'Living Rooms', 'Post Type Singular Name', 'twentytwenty' ),
        'menu_name'           => __( 'Living Rooms Gallery', 'twentytwenty' ),
        'parent_item_colon'   => __( 'Parent Living Rooms', 'twentytwenty' ),
        'all_items'           => __( 'All Living Rooms', 'twentytwenty' ),
        'view_item'           => __( 'View Living Rooms', 'twentytwenty' ),
        'add_new_item'        => __( 'Add New Living Rooms', 'twentytwenty' ),
        'add_new'             => __( 'Add New', 'twentytwenty' ),
        'edit_item'           => __( 'Edit Living Rooms', 'twentytwenty' ),
        'update_item'         => __( 'Update Living Rooms', 'twentytwenty' ),
        'search_items'        => __( 'Search Living Rooms', 'twentytwenty' ),
        'not_found'           => __( 'Not Found', 'twentytwenty' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Living Rooms', 'twentytwenty' ),
        'description'         => __( 'Living Rooms Gallery', 'twentytwenty' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
     
    // Registering your Custom Post Type
    register_post_type( 'Living Rooms', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type_living', 0 );


/*
* Creating a function to create our CPT
*/
 
function custom_post_type_Dining() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Dining Rooms Gallery', 'Post Type General Name', 'twentytwenty' ),
        'singular_name'       => _x( 'Dining Rooms', 'Post Type Singular Name', 'twentytwenty' ),
        'menu_name'           => __( 'Dining Rooms Gallery', 'twentytwenty' ),
        'parent_item_colon'   => __( 'Parent Dining Rooms', 'twentytwenty' ),
        'all_items'           => __( 'All Dining Rooms', 'twentytwenty' ),
        'view_item'           => __( 'View Dining Rooms', 'twentytwenty' ),
        'add_new_item'        => __( 'Add New Dining Rooms', 'twentytwenty' ),
        'add_new'             => __( 'Add New', 'twentytwenty' ),
        'edit_item'           => __( 'Edit Dining Rooms', 'twentytwenty' ),
        'update_item'         => __( 'Update Dining Rooms', 'twentytwenty' ),
        'search_items'        => __( 'Search Dining Rooms', 'twentytwenty' ),
        'not_found'           => __( 'Not Found', 'twentytwenty' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Dining Rooms', 'twentytwenty' ),
        'description'         => __( 'Dining Rooms Gallery', 'twentytwenty' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
     
    // Registering your Custom Post Type
    register_post_type( 'Dining Rooms', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type_Dining', 0 );

/*
* Creating a function to create our CPT
*/
 
function custom_post_type_Bedrooms() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Bedroomss Gallery', 'Post Type General Name', 'twentytwenty' ),
        'singular_name'       => _x( 'Bedroomss', 'Post Type Singular Name', 'twentytwenty' ),
        'menu_name'           => __( 'Bedroomss Gallery', 'twentytwenty' ),
        'parent_item_colon'   => __( 'Parent Bedroomss', 'twentytwenty' ),
        'all_items'           => __( 'All Bedroomss', 'twentytwenty' ),
        'view_item'           => __( 'View Bedroomss', 'twentytwenty' ),
        'add_new_item'        => __( 'Add New Bedroomss', 'twentytwenty' ),
        'add_new'             => __( 'Add New', 'twentytwenty' ),
        'edit_item'           => __( 'Edit Bedroomss', 'twentytwenty' ),
        'update_item'         => __( 'Update Bedroomss', 'twentytwenty' ),
        'search_items'        => __( 'Search Bedroomss', 'twentytwenty' ),
        'not_found'           => __( 'Not Found', 'twentytwenty' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Bedroomss', 'twentytwenty' ),
        'description'         => __( 'Bedroomss Gallery', 'twentytwenty' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
     
    // Registering your Custom Post Type
    register_post_type( 'Bedrooms', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type_Bedrooms', 0 );

/* Gallery shortcode  */
function kitchen_gallery() {  

    $args = array(
        'post_type' => 'kitchens',
        'posts_per_page' => 15
        // Several more arguments could go here. Last one without a comma.
    );
    // Query the posts:
    $query = new WP_Query($args);
    $posts = $query->posts;
    // Loop through the obituaries:
    $content_list = "";
    $content_list .= '<div class="row galleryRow">';
    foreach($posts as $post) {
        
        $content_list .= '<div class="ImageWrapper" class="col-lg-6">';
        $content_list .= '<div class="imageHolder">';

        $images = get_field('gallery', $post->ID);
        $link = get_field('link', $post->ID);
		
        if (!empty($images)){
        foreach( $images as $image ): 
            
            $content_list .= '<div class="img-responsive toggle-image" style="background-image:url('.esc_url($image['url']).');background-size: 100% 100%;background-position:center" data-targetimg="gallery_item_0" data-responsive="'.esc_url($image['url']).'" data-src="'.esc_url($image['url']).'" data-exthumbimage="'.esc_url($image['url']).'">';
            $content_list .= '<a href="javascript:void(0)" class="popup-overlay-link"></a>';
            $content_list .= '<span class="main-imgs"><img src="'.esc_url($image['url']).'" class="img-responsive toggle-image" alt="'.get_the_title( $post ).'" /></span>';
            $content_list .= '</div>';
            break;
        endforeach;  
    } 

        if( $images ): 

        $gal_count = 1;
        foreach( $images as $image ): 
            if($gal_count > 1){
                            $content_list .= '<div class="popup-imgs-holder" data-targetimg="gallery_item_'.$gal_count.'" data-responsive="'.esc_url($image['url']).'" data-exthumbimage="'.esc_url($image['sizes']['thumbnail']).'" data-src="'.esc_url($image['url']).'">';
            $content_list .= '<a href="javascript:void(0)" title="'. esc_url($image['alt']).'">';
            $content_list .= '<span class="main-imgs">';
            $content_list .= '<img src="'.esc_url($image['url']).'" alt="'. get_the_title( $post ).'" /></span></a></div>';
            }
           $gal_count++;

         endforeach;    
        endif;
        $content_list .= '</div>';
        $content_list .= '<div class="viewFlooring"><a href="'.esc_url($link).'" target="_self"> view this flooring </a></div>';


        $content_list .= '</div>';
    }
    $content_list .= '</div>';
    
    echo $content_list;
    // Reset Post Data
}

add_shortcode( 'kitchen_gallery', 'kitchen_gallery');


/* Living Gallary  */
function living_gallery() {  

    $args = array(
        'post_type' => 'livingrooms',
        'posts_per_page' => 15
        // Several more arguments could go here. Last one without a comma.
    );
    // Query the posts:
    $query = new WP_Query($args);
    $posts = $query->posts;
    // Loop through the obituaries:
    $content_list = "";
    $content_list .= '<div class="row galleryRow">';
    foreach($posts as $post) {
        
        $content_list .= '<div class="ImageWrapper" class="col-lg-6">';
        $content_list .= '<div class="imageHolder">';

        $images = get_field('gallery', $post->ID);
        $link = get_field('link', $post->ID);
		
        if (!empty($images)){
        foreach( $images as $image ): 
            
            $content_list .= '<div class="img-responsive toggle-image" style="background-image:url('.esc_url($image['url']).');background-size: 100% 100%;background-position:center" data-targetimg="gallery_item_0" data-responsive="'.esc_url($image['url']).'" data-src="'.esc_url($image['url']).'" data-exthumbimage="'.esc_url($image['url']).'">';
            $content_list .= '<a href="javascript:void(0)" class="popup-overlay-link"></a>';
            $content_list .= '<span class="main-imgs"><img src="'.esc_url($image['url']).'" class="img-responsive toggle-image" alt="'.get_the_title( $post ).'" /></span>';
            $content_list .= '</div>';
            break;
        endforeach;  
    } 

        if( $images ): 

        $gal_count = 1;
        foreach( $images as $image ): 
            if($gal_count > 1){
                            $content_list .= '<div class="popup-imgs-holder" data-targetimg="gallery_item_'.$gal_count.'" data-responsive="'.esc_url($image['url']).'" data-exthumbimage="'.esc_url($image['sizes']['thumbnail']).'" data-src="'.esc_url($image['url']).'">';
            $content_list .= '<a href="javascript:void(0)" title="'. esc_url($image['alt']).'">';
            $content_list .= '<span class="main-imgs">';
            $content_list .= '<img src="'.esc_url($image['url']).'" alt="'. get_the_title( $post ).'" /></span></a></div>';
            }
           $gal_count++;

         endforeach;    
        endif;
        $content_list .= '</div>';
        $content_list .= '<div class="viewFlooring"><a href="'.esc_url($link).'" target="_self"> view this flooring </a></div>';


        $content_list .= '</div>';
    }
    $content_list .= '</div>';
    
    echo $content_list;
    // Reset Post Data

}

add_shortcode( 'living_gallery', 'living_gallery');


/* Dining Gallary  */
function dining_gallery() {  

    $args = array(
        'post_type' => 'diningrooms',
        'posts_per_page' => 15
        // Several more arguments could go here. Last one without a comma.
    );
    // Query the posts:
    $query = new WP_Query($args);
    $posts = $query->posts;
    // Loop through the obituaries:
    $content_list = "";
    $content_list .= '<div class="row galleryRow">';
    foreach($posts as $post) {
        
        $content_list .= '<div class="ImageWrapper" class="col-lg-6">';
        $content_list .= '<div class="imageHolder">';

        $images = get_field('gallery', $post->ID);
        $link = get_field('link', $post->ID);
		
        if (!empty($images)){
        foreach( $images as $image ): 
            
            $content_list .= '<div class="img-responsive toggle-image" style="background-image:url('.esc_url($image['url']).');background-size: 100% 100%;background-position:center" data-targetimg="gallery_item_0" data-responsive="'.esc_url($image['url']).'" data-src="'.esc_url($image['url']).'" data-exthumbimage="'.esc_url($image['url']).'">';
            $content_list .= '<a href="javascript:void(0)" class="popup-overlay-link"></a>';
            $content_list .= '<span class="main-imgs"><img src="'.esc_url($image['url']).'" class="img-responsive toggle-image" alt="'.get_the_title( $post ).'" /></span>';
            $content_list .= '</div>';
            break;
        endforeach;  
    } 

        if( $images ): 

        $gal_count = 1;
        foreach( $images as $image ): 
            if($gal_count > 1){
                            $content_list .= '<div class="popup-imgs-holder" data-targetimg="gallery_item_'.$gal_count.'" data-responsive="'.esc_url($image['url']).'" data-exthumbimage="'.esc_url($image['sizes']['thumbnail']).'" data-src="'.esc_url($image['url']).'">';
            $content_list .= '<a href="javascript:void(0)" title="'. esc_url($image['alt']).'">';
            $content_list .= '<span class="main-imgs">';
            $content_list .= '<img src="'.esc_url($image['url']).'" alt="'. get_the_title( $post ).'" /></span></a></div>';
            }
           $gal_count++;

         endforeach;    
        endif;
        $content_list .= '</div>';
        $content_list .= '<div class="viewFlooring"><a href="'.esc_url($link).'" target="_self"> view this flooring </a></div>';


        $content_list .= '</div>';
    }
    $content_list .= '</div>';
    
    echo $content_list;
    // Reset Post Data

}

add_shortcode( 'dining_gallery', 'dining_gallery');


/* Bedroom  Gallary  */
function bedroom_gallery() {  

    $args = array(
        'post_type' => 'bedrooms',
        'posts_per_page' => 15
        // Several more arguments could go here. Last one without a comma.
    );
    // Query the posts:
    $query = new WP_Query($args);
    $posts = $query->posts;
    // Loop through the obituaries:
    $content_list = "";
    $content_list .= '<div class="row galleryRow">';
    foreach($posts as $post) {
        
        $content_list .= '<div class="ImageWrapper" class="col-lg-6">';
        $content_list .= '<div class="imageHolder">';

        $images = get_field('gallery', $post->ID);
        $link = get_field('link', $post->ID);
		
        if (!empty($images)){
        foreach( $images as $image ): 
            
            $content_list .= '<div class="img-responsive toggle-image" style="background-image:url('.esc_url($image['url']).');background-size: 100% 100%;background-position:center" data-targetimg="gallery_item_0" data-responsive="'.esc_url($image['url']).'" data-src="'.esc_url($image['url']).'" data-exthumbimage="'.esc_url($image['url']).'">';
            $content_list .= '<a href="javascript:void(0)" class="popup-overlay-link"></a>';
            $content_list .= '<span class="main-imgs"><img src="'.esc_url($image['url']).'" class="img-responsive toggle-image" alt="'.get_the_title( $post ).'" /></span>';
            $content_list .= '</div>';
            break;
        endforeach;  
    } 

        if( $images ): 

        $gal_count = 1;
        foreach( $images as $image ): 
            if($gal_count > 1){
                            $content_list .= '<div class="popup-imgs-holder" data-targetimg="gallery_item_'.$gal_count.'" data-responsive="'.esc_url($image['url']).'" data-exthumbimage="'.esc_url($image['sizes']['thumbnail']).'" data-src="'.esc_url($image['url']).'">';
            $content_list .= '<a href="javascript:void(0)" title="'. esc_url($image['alt']).'">';
            $content_list .= '<span class="main-imgs">';
            $content_list .= '<img src="'.esc_url($image['url']).'" alt="'. get_the_title( $post ).'" /></span></a></div>';
            }
           $gal_count++;

         endforeach;    
        endif;
        $content_list .= '</div>';
        $content_list .= '<div class="viewFlooring"><a href="'.esc_url($link).'" target="_self"> view this flooring </a></div>';


        $content_list .= '</div>';
    }
    $content_list .= '</div>';
    
    echo $content_list;
    // Reset Post Data

}

add_shortcode( 'bedroom_gallery', 'bedroom_gallery');


/**
 * @snippet       Clear the checkout fields. 
 */
add_filter('woocommerce_checkout_get_value','__return_empty_string',10);

// add_action( 'woocommerce_after_add_to_cart_button', 'add_content_after_addtocart_button_func' );
add_action( 'woocommerce_product_meta_start', 'add_content_after_addtocart_button_func', 10, 1 );  
function add_content_after_addtocart_button_func() {
        echo '<div class="find_retailer_wrapper"><h4>Find A Retailer Near Me</h4>';
        echo do_shortcode('[gravityform id="30" title="false" description="false" ajax="true"]');
        echo '</div>';
}

/*
function woocom_api_export()
{
    global $wpdb;   

		
		// template for inserting the shop_order posts
$post_sql_template = "INSERT INTO `wp_posts` (
            `post_author`,
            `post_date`,
            `post_date_gmt`,
            `post_content`,
            `post_title`,
            `post_excerpt`,
            `post_status`,
            `comment_status`,
            `ping_status`,
            `post_password`,
            `post_name`,
            `to_ping`,
            `pinged`,
            `post_modified`,
            `post_modified_gmt`,
            `post_content_filtered`,
            `post_parent`,
            `guid`,
            `menu_order`,
            `post_type`,
            `post_mime_type`,
            `comment_count`
        ) VALUES (
            %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', %s, '%s', '%s', %s);";

// template for inserting the order_items
$item_sql_template = "INSERT INTO `wp_woocommerce_order_items` (`order_item_name`, `order_item_type`, `order_id`) VALUES ('%s', '%s', '%s')";

// If you want to remove all orders in the target db, and replace with those from source, uncomment this block.
// I don't usually do this as leaving it commented, orders common to both systems will be skipped and so
// retain their original ids

// $sql = "DELETE FROM wp_woocommerce_order_itemmeta";
// mysqli_query($new_conn, $sql);
// $sql = "DELETE FROM wp_woocommerce_order_items";
// mysqli_query($new_conn, $sql);
// $sql = "DELETE FROM wp_posts WHERE post_type = 'shop_order'";
// mysqli_query($new_conn, $sql);

$sql = "SELECT * FROM wp_posts_1 WHERE post_type = 'shop_order' ";
$duplicates = $wpdb->get_results($sql,ARRAY_A);	
//print_r($duplicates);exit;
//$result_first = $wpdb->query($sql);

foreach($duplicates as $row)
{

$old_id = $row['ID'];
    // check to see if a shop_order post with this id exists - if so, skip
    // (see note on deleting above - this won't happen if that block is uncommented)
	 

	
    // add the post
    $post_sql = sprintf($post_sql_template,
        $row['post_author'],
        $row['post_date'],
        $row['post_date_gmt'],
        $row['post_content'],
        $row['post_title'],
        $row['post_excerpt'],
        $row['post_status'],
        $row['comment_status'],
        $row['ping_status'],
        $row['post_password'],
        $row['post_name'],
        $row['to_ping'],
        $row['pinged'],
        $row['post_modified'],
        $row['post_modified_gmt'],
        $row['post_content_filtered'],
        $row['post_parent'],
        $row['guid'],
        $row['menu_order'],
        $row['post_type'],
        $row['post_mime_type'],
        $row['comment_count']
    );
	$sql_insert = $wpdb->query($post_sql);
	//$wpdb->query($sql_insert);
    //$insert_post_res = mysqli_query($post_sql);
    $new_id = $wpdb->insert_id;


    // and the postmeta
    $sql_meta =  $wpdb->query("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) SELECT " . $new_id . ", meta_key, meta_value FROM wp_postmeta_1 old WHERE old.post_id = " . $old_id);
	//$wpdb->query($sql_meta);
  

    

    // and then order items and order item meta
	$sql_order_item = sprintf("SELECT * FROM wp_woocommerce_order_items_1 WHERE order_id = %s", $old_id);
	$order_item_row = $wpdb->get_results($sql_order_item,ARRAY_A);	
    
    foreach($order_item_row as $item_row)
{
        $old_item_id = $item_row['order_item_id'];

        $item_sql1 = sprintf($item_sql_template,
            $item_row['order_item_name'],
            $item_row['order_item_type'],
            $new_id
        );
		$item_sql = $wpdb->query($item_sql1);
		//$wpdb->query($item_sql);
        $new_item_id = $wpdb->insert_id;
        $sql_meta = $wpdb->query("INSERT INTO wp_woocommerce_order_itemmeta (order_item_id, meta_key, meta_value) SELECT " . $new_item_id . ", meta_key, meta_value FROM wp_woocommerce_order_itemmeta_1 old WHERE old.order_item_id = " . $old_item_id);
        //$wpdb->query($sql_meta);

    }
 
}  



global $wpdb;

$duplicate_titles = $wpdb->get_col("SELECT post_title FROM wp_posts WHERE `post_type` = 'shop_order' GROUP BY post_title HAVING COUNT(*) > 1");

foreach( $duplicate_titles as $title ) {
    $post_ids = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM wp_posts  WHERE post_title=%s", $title ) ); 
    // Iterate over the second ID with this post title till the last
    foreach( array_slice( $post_ids, 1 ) as $post_id ) {
        wp_delete_post( $post_id, true ); // Force delete this post
    }
}
}
add_shortcode( 'woocom_api_export', 'woocom_api_export');
*/

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   
}
add_filter( 'auto_update_plugin', '__return_false' );

remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 20);
